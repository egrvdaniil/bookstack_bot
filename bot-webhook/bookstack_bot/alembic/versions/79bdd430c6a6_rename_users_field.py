"""rename users field

Revision ID: 79bdd430c6a6
Revises: e33ce4b84e23
Create Date: 2022-02-21 12:22:20.497749

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "79bdd430c6a6"
down_revision = "e33ce4b84e23"
branch_labels = None
depends_on = None


def upgrade():
    op.alter_column("users", "user_email", new_column_name="username")


def downgrade():
    op.alter_column("users", "username", new_column_name="user_email")

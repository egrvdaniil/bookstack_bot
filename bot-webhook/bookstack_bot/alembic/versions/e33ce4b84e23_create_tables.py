"""create tables

Revision ID: e33ce4b84e23
Revises: 
Create Date: 2022-02-17 17:54:43.478096

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "e33ce4b84e23"
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "users",
        sa.Column("chat_id", sa.String(50), primary_key=True),
        sa.Column("user_email", sa.String(50), nullable=False),
    )


def downgrade():
    op.drop_table("users")

from hmac import compare_digest
from models import Users
from sqlalchemy.future import select
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import sessionmaker
import telebot
from telebot.async_telebot import AsyncTeleBot
from sqlalchemy.ext.asyncio import AsyncSession


class BotHandlers:
    def __init__(self, bot: AsyncTeleBot, session_maker: sessionmaker, bot_auth_code):
        self.bot = bot
        self.session_maker = session_maker
        self.bot_auth_code = bot_auth_code

    async def bot_start(
        self,
        message: telebot.types.Message,
    ):
        auth_code = message.text.split(" ")[1]
        if not compare_digest(auth_code, self.bot_auth_code):
            await self.bot.reply_to(
                message,
                "Неправильный код доступа, запросите у администратора ссылку на доступ.",
            )
            return
        async with self.session_maker() as session:
            username = message.from_user.username
            if username is None:
                username = str(message.from_user.id)
            user = Users(
                chat_id=str(message.chat.id), username=username
            )
            session.add(user)
            try:
                await session.commit()
            except IntegrityError:
                await self.bot.reply_to(message, "Вы уже аворизированны")
                return
            await self.bot.reply_to(message, "Добро пожаловать")

    async def echo_message(self, message: telebot.types.Message):
        await self.bot.reply_to(message, message.text)

    async def add_webhook(self, url, ssl_sert):
        await self.bot.remove_webhook()
        await self.bot.set_webhook(url=url, certificate=open(ssl_sert, "r"))

    def register(self):
        self.bot.message_handler(commands=["start"])(self.bot_start)
        self.bot.message_handler(content_types=["text"])(self.echo_message)

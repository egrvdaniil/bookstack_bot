#!/bin/sh

set -e

. /venv/bin/activate

if [ ! -d "../app_data/serts" ]; then
  cd ../app_data
  mkdir serts
  cd serts

  openssl req -newkey rsa:2048 -sha256 -nodes -keyout server.key -x509 -days 3650 -out server.crt -subj "/C=RU/CN=$HOSTNAME" -addext "subjectAltName = DNS:$HOSTNAME"

  cd ../../bookstack_bot
fi


alembic upgrade head
python start.py

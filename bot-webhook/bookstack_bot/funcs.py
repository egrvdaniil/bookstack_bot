import httpx
from dataclasses import dataclass
from markdownify import MarkdownConverter


class ImageBlockConverter(MarkdownConverter):
    def convert_img(self, el, text, convert_as_inline):
        src = el.attrs.get("src", None) or ""
        return f"\n{src}\n"


def md(html, **options):
    return ImageBlockConverter(**options).convert(html)


@dataclass
class PageInfo:
    book_name: str
    book_slug: str
    page_name: str
    page_slug: str
    page_html: str

    def to_message(self, book_stack_url: str) -> str:
        message = (
            f"Новая страница в разделе {self.book_name}\n\n"
            + f"* [{self.page_name}]({book_stack_url}books/{self.book_slug}/page/{self.page_slug}) *\n\n"
            + md(self.page_html)
        )
        if len(message) > 4000:
            message = (
                message[:4000]
                + f"...\n\n[Полная версия]({book_stack_url}books/{self.book_slug}/page/{self.page_slug})"
            )

        return message


async def get_page_info(bookstack_url: str, page_id: int, token: str) -> PageInfo:
    headers = {"Accept": "application/json", "Authorization": f"Token {token}"}
    async with httpx.AsyncClient(headers=headers) as client:
        client: httpx.AsyncClient()
        res = await client.get(bookstack_url + f"api/pages/{page_id}")
        page_data = res.json()
        book_id = page_data["book_id"]
        res = await client.get(bookstack_url + f"api/books/{book_id}")
        book_data = res.json()

    page_info = PageInfo(
        book_name=book_data["name"],
        book_slug=book_data["slug"],
        page_name=page_data["name"],
        page_slug=page_data["slug"],
        page_html=page_data["html"],
    )
    return page_info

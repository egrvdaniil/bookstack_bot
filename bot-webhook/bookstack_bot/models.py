from sqlalchemy.ext.declarative import declarative_base
import sqlalchemy as sa

Base = declarative_base()


class Users(Base):
    __tablename__ = 'users'
    chat_id = sa.Column(sa.String(50), primary_key=True)
    username = sa.Column(sa.String(50), nullable=False)

from sqlalchemy.ext.asyncio import create_async_engine, AsyncSession
from sqlalchemy.orm import sessionmaker
from sanic import Sanic
from sanic.request import Request
from sanic.response import text, empty
import telebot
from telebot.async_telebot import AsyncTeleBot
import asyncio
import os
import bot_funcs
from sqlalchemy.future import select
from pprint import pprint
from funcs import get_page_info
from models import Users

TELEGRAM_TOKEN = os.environ.get("TELEGRAM_TOKEN")
BOOKSTACK_API_TOKEN = os.environ.get("BOOKSTACK_API_TOKEN")
BOOKSTACK_API_URL = os.environ.get("BOOKSTACK_API_URL")
BOT_AUTH_CODE = os.environ["BOT_AUTH_CODE"]
WEBHOOK_SSL_CERT = "../app_data/serts/server.crt"
WEBHOOK_SSL_PRIV = "../app_data/serts/server.key"
WEBHOOK_PORT = os.environ.get("PORT")
WEBHOOK_URL_BASE = "https://" + os.environ.get("HOSTNAME") + ":" + WEBHOOK_PORT
WEBHOOK_URL_PATH = "/telegram"


engine = create_async_engine("sqlite+aiosqlite:///../app_data/data.db")
SessionLocal = sessionmaker(bind=engine, expire_on_commit=False, class_=AsyncSession)


app = Sanic("MyHelloWorldApp")

bot = AsyncTeleBot(TELEGRAM_TOKEN, parse_mode="MARKDOWN")


@app.get("/health_check")
async def foo_handler(request: Request):
    return text("ok")


@app.post("/bookstack_page_created")
async def bookstack_page_created(request: Request):
    webhook_data = request.json
    page_id = webhook_data["related_item"]["id"]
    page_info = await get_page_info(BOOKSTACK_API_URL, page_id, BOOKSTACK_API_TOKEN)
    async with SessionLocal() as session:
        users = await session.execute(select(Users))
        users_list = users.scalars().all()
        for user in users_list:
            await request.app.add_task(
                bot.send_message(user.chat_id, page_info.to_message(BOOKSTACK_API_URL))
            )

    return text("ok")


@app.post(WEBHOOK_URL_PATH)
async def foo_handler(request: Request):
    update = telebot.types.Update.de_json(request.json)
    await bot.process_new_updates([update])
    return empty()


bot_handlers = bot_funcs.BotHandlers(bot, SessionLocal, BOT_AUTH_CODE)
bot_handlers.register()

asyncio.run(
    bot_handlers.add_webhook(
        url=WEBHOOK_URL_BASE + WEBHOOK_URL_PATH, ssl_sert=WEBHOOK_SSL_CERT
    )
)

ssl = {
    "cert": WEBHOOK_SSL_CERT,
    "key": WEBHOOK_SSL_PRIV,
}
app.run(host="0.0.0.0", port=8443, ssl=ssl, workers=1)

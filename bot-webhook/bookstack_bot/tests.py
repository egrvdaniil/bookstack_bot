from unittest import IsolatedAsyncioTestCase, main
from models import Base, Users
import bot_funcs
from sqlalchemy.ext.asyncio import create_async_engine, AsyncSession
from sqlalchemy.orm import sessionmaker
from unittest.mock import Mock, MagicMock
from sqlalchemy.future import select
from sqlalchemy import delete
from funcs import get_page_info
import os


class AsyncMock(MagicMock):
    async def __call__(self, *args, **kwargs):
        return super(AsyncMock, self).__call__(*args, **kwargs)


class TestBot(IsolatedAsyncioTestCase):
    def setUp(self):
        self.engine = create_async_engine("sqlite+aiosqlite:///:memory:")
        self.session = sessionmaker(
            bind=self.engine, expire_on_commit=False, class_=AsyncSession
        )

    async def asyncSetUp(self) -> None:
        async with self.engine.begin() as conn:
            await conn.run_sync(Base.metadata.drop_all)
            await conn.run_sync(Base.metadata.create_all)

    async def asyncTearDown(self):
        async with self.engine.begin() as conn:
            await conn.run_sync(Base.metadata.drop_all)
        await self.engine.dispose()

    async def test_bot_start(self):
        passcode = "test_test"
        username = "user"
        bot = AsyncMock()
        handlers = bot_funcs.BotHandlers(bot, self.session, passcode)
        message = Mock()
        message.text = "/start " + passcode
        message.chat.id = 123
        message.from_user.username = username
        await handlers.bot_start(message)
        self.assertEqual(bot.reply_to.call_count, 1)
        self.assertEqual(bot.reply_to.call_args[0][1], "Добро пожаловать")
        async with self.session() as session:
            res = await session.execute(select(Users).where(Users.chat_id == "123"))
            self.assertEqual(username, res.one()[0].username)

        await handlers.bot_start(message)
        self.assertEqual(
            bot.reply_to.call_args[0][1],
            "Вы уже аворизированны",
        )

        message.text = "/start " + "wrong pass"
        await handlers.bot_start(message)
        self.assertEqual(
            bot.reply_to.call_args[0][1],
            "Неправильный код доступа, запросите у администратора ссылку на доступ.",
        )

        await session.execute(delete(Users).where(Users.chat_id == "123"))


if __name__ == "__main__":
    main()
